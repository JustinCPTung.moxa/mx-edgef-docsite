# Use Case

## Product Plugin Development

多數場景 MOXA Shared API 已經可以滿足設備管理的需求．在這樣的狀況下，產品單位將專注於不同產品所要提供的應用服務．例如Protocol相關產品，著重於 protocol service的整合．IoT相關產品，著重於邊緣運算，以及資料與雲端服務的連結．本框架中Plugin service提供兩種服務性質整合 `systemd service` `container service` ．下圖示範服務與框架如何整合成為最終產品樣貌．

![](assets/usecase-product-service.png)

## Core Function Extension

在框架中，我們瞭解到開發者需要直接而有效率地操作核心功能。因此提供了MOXA Shared API，開發者能夠輕鬆存取和操控我們框架的核心功能，從而加速開發過程並提升產品效能。然而隨著項目需求的增長，開發者可能會需要擴展或自定義功能以滿足特定的需求。因此，我們的框架被設計為高度可擴充的，尤其在三個核心層次：ACL、CFL、以及OSL。每一層都提供了相應的擴充方法，讓開發者能夠依據自己的需求進行整合和擴展。

![](assets/usecase-system-integration.png)

## Co-exist Service Integration

### Service Permitted Access
![](assets/usecase-service-register.png)

### Debian Package

```
debian
├── /var/thingspro/services/modules/{SERVICE}.json
├── /var/thingspro/services/nginx/{SERVICE}.conf
├── /var/thingspro/services/permissions/{SERVICE}.yaml
├── /lib/systemd/system/{SERVICE}.service
├── /usr/sbin/{SERVICE_BINARY}
└── **/var/www/apps/edge-web/**
```
- modules → 服務註冊到框架時提供的Profile．
    
    ```json
    {
    	"name": "logic-engine",
        "displayName": "Logic Engine",
        "service": "moxa-logic-engine.service",
        "debName": "moxa-logic-engine"
    }
    ```
    
- nginx → 服務所需要API相關的設定．
    
    ```nix
    location ^~ /api/v1/mqtt {
        set $auth_request "APP_MQTT_RW";
        auth_request /api/v1/auth;
        auth_request_set $auth_status $upstream_status;
        auth_request_set $auth_username $upstream_http_x_auth_username;
    
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header Host $host;
        proxy_set_header X-Auth-Username $auth_username;
        proxy_pass http://unix:/run/tpe/cloud/connMgmt.sock;
    }
    ```
    
- permission → 服務宣告對框架中Permission的要求
    
    ```yaml
    kind: srv-permission
    version: v1
    spec:
      name: cloud
      displayname: Cloud Service
      addon:
        - id: APP_AZURE_DEVICE_RW
          name: Azure IoT Device
        - id: APP_MQTT_RW
          name: MQTT Client
      required:
        - SYS_MAINTENANCE_RW
        - SYS_MANAGEMENT_RW
        - SYS_TAGHUB_RW
        - SYS_SECURITY_RW
    
    ```
> ⚠️⚠️⚠️ 🧑‍💻
由於先前網頁尚未導入micro-web．因此現階段無法獨立打包服務網頁，還是得透過更新整包框架web．會再與F2E討論相關導入時程．
> 