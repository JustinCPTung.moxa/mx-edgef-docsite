---
slug: /
sidebar_position: 1
---

# About

Moxa Edge framework 將過往IOT產品所開發的插件服務管理、硬體介面整合、things應用資料串接、和部署維護等核心功能，整合為一個邊緣計算開放框架．簡單來說，就是成為 `OT設備採集` 與 `IT資料處理` 的邊緣中介軟體，定義明確分層架構，提供採集設備一站式環境整合，高彈性、互操作性資料處理服務．使開發人員基於框架更專注於產品邊緣邏輯開發，加速產品迭代。

![](assets/mef-product-map.png)


## 技術發展策略與目標
### RDC 五年共用平台策略

目標實現IPC一致性設備管理平台與 MOXA API 導入，打造軟體整合大部隊。

### 分層堆疊架構與分工明確

透過框架分層式架構，ACL (Access Layer)、CFL (Core Function Layer)、和OSL (Operation System Layer)，減少模糊地帶。作為IPC產品明確對上對下介接的中介軟體。

### 支持MIL產品週期

- Arm-base IPC
    - MIL 1
    - MIL 3

### 支援工業安全規範

- SDLC-D
- IEC 62443-4-1
- IEC 62443-4-2
- IEC 62443 ICSA