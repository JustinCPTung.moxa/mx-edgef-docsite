# Install Web

The aig-302 web material is able to install for making Moxa-Edge-Manager operate by Web UI

Install web ui: [moxa-edge-manager-web_0.2.4_all.deb](https://moxa-my.sharepoint.com/:f:/p/jamescw_kao/EgHDwqax4RtDkBGAxdGrYbUBzuMKymI_NZE8CofZXahllg?e=eF0riE)

```
apt-get install ./moxa-edge-manager-web_0.2.4_all.deb
```

Open the browser and enter the URL: `https://{DUT_IP}:8443`
Now, you're able to configure, operate, maintain MIL3 IPC easily through Web UI

![](web.png)