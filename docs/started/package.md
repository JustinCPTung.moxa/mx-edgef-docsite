# Build package

```
ARCH=armhf Platform=MIL3 make local-deb
```

The debian package version is composed by `${REVISION}`-`${PLATFORM}`

```
moxa-edge-manager_0.0.1-mil3+latest_armhf.deb
```

## Installation
Using the MIL3 platform as an example, install and run as a service on it!

```
apt-get update
apt-get install -f ./moxa-edge-manager_0.0.1-mil3+latest_armhf.deb
```

## Service Control
```
systemctl start/stop/status moxa-edge-manager
```
![alt text](startstopstatus.png)


## Log
Checkout the service log started successfully
```
journalctl -u moxa-edge-manager -af
```

![alt text](syslog.png)