# Quick start

## Prerequisites

As the Moxa Edge framework is still in its early stages, we recommend utilizing Gitlab Codespaces as a quick way to stay updated with the latest version. The develop branch will consistently serve as the preview version.

```
git clone git@gitlab.com:moxa/ibg/software/platform/thingspro/moxa-edgef.git
```


Before getting started with the Moxa Edge Framework, ensure that your environment meets the following requirements:

**Environment**
- **Operating System:** MIL3
- **Golang:** Version 1.19 or higher

**3rd-party Software Dependency**
- **Docker CE:** Version 18.04 or higher
- **Nginx:** Version 1.18.0-6.1

    To check version:

    ```
    # dpkg -l | grep nginx
    # nginx-light, libnginx-mod-http-headers-more-filter
    ```


**MIL Software Dependency**
- **Moxa Connection Manager (MCM):** Version 1.4.10

    To check version:
    
    `mx-connect-mgmt -V`

- **Moxa Data Exchange:** Version 4.2.6 or higher

    To Install:
    ```
    wget http://ibg-edge-s3.moxa.com/v3/edge/builds/tpe3/3rd-party/latest/libmx-dx1_4.2.6_armhf.deb
    wget http://ibg-edge-s3.moxa.com/v3/edge/builds/tpe3/3rd-party/latest/libmx-dx-dev_4.2.6_armhf.deb

    apt-get install -f ./libmx-dx1_4.2.6_armhf.deb ./libmx-dx-dev_4.2.6_armhf.deb
    ```
    To check version:
    
    `dpkg -l | grep libmx-dx`


## Build

### Build tools
- make
- build-essential

```
apt-get install make build-essential
```

### Prepare the Builder
Set up environment variables and build builder first. This step ONLY do once before builder changed.
Set armhf, arm64, or amd64 according to your target device.

ARCH options:
 - `armhf`
 - `armf64`
 - `amd64`

Platform options:
 - `MIL1`
 - `MIL3`


For example:
```
ARCH=${TARGET_ARCH} PLATFORM=MIL1 make builder
```

### Build Binary
```
make armhf/mem
```

**Cross Building**
> ⚠️⚠️⚠️ 🧑‍💻
Due to Golang design and tooling, it can compile code for different target architectures and operating systems. The framework define relevant compiling configuration which is the architecture env mentioned as above ($ARCH). Besides the flag, the 3rd-party dependencies for different archs are required as well.
>

```
apt-get install crossbuild-essential-armhf
apt-get install nginx-light:armhf, libnginx-mod-http-headers-more-filter:armhf
``` 


## Run as a Daemon
Once you finish all the above procedures. Now the binary is ready for running as a daemon in the system.

```
./build/${ARCH}/sysman daemon
```


During the program booting, there are several sub-processes will be bring up as well, but the most important one you can check if the following logs come up to the console after the initialization finished which means the program started and is running as well.

```
Mar 19 09:52:16 moxa sysman[29338]: ╔════════════════════════════════════════════════════════╗
Mar 19 09:52:16 moxa sysman[29338]: ║ ThingsPro Edge is Ready
Mar 19 09:52:16 moxa sysman[29338]: ╚════════════════════════════════════════════════════════╝
```
