# What is Moxa Edge Framework?

## Architecture overview

![](../assets/mef-layers.png)

There are three core layers in the Moxa Edge Framework architecture:

**ACL (Access Layer)**

- 使用`API`作為使用者，以及服務與核心服務之間標準溝通層
- Moxa Shared API 由 MEF Team 負責維護
- Product Team 可依新需求擴充 API

**CFL (Core Function Layer)**

- 由多個服務包含System, Data, Security, 和Plugin組成，負責框架核心功能．
- MEF Team 負責 Core Function Service 開發以及維護．

**OSL (Operation System Layer)**

- 作業系統整合介面層，統一性的管理多種IPC平台規格．
- MEF Team 負責 OSI 層介面制定，並與 MIL Team協作維護．
- Product Team 可依新需求擴充 OSI．

## Validated environments
Currently, Edge team has validated Moxa Edge Fraemwork against the following fixed-set of MIL version and HW device:
|   MIL  |  Model  |
| :----: | :-----: |
|  MIL3  | UC-8200 |
|  MIL1  | UC-3100 |

## License

Moxa License